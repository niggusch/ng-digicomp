# Preparation JS Workshop

## Important
Make sure that you have unrestricted access to the internet! In the steps below you will have to access GitHub and npmjs.org. Some corporate proxies block these sites or access over https/git!

## Software Installation
For the workshop the following software should be installed.  
**The version numbers don't have to match exactly, but should be pretty close!**

### Git
A recent git installation is needed to download the exercise files. Git can be downloaded from here: <https://git-scm.com/download/>

Check:  

	> git --version                                                             
	git version 2.6.1



### Node.js & NPM 
Node and NPM are the fundament for the JavaScript toolchain.
The Node installer can be downloaded here: <http://nodejs.org/download/>

**Advanced instructions for OSX/Linux:** If you don't want to install global packages with `sudo` in the following steps, you can perform the instructions here: <https://docs.npmjs.com/getting-started/fixing-npm-permissions>


**Check**:

	> node --version
	v4.1.1
	> npm --version
	2.14.4


### Global NPM Packages

We want to install some JavaScript development tools globally, so that they can be used from the commandline. As default (if you did not execute the advanced instructions above to avoid `sudo`) the installation of those tools is placed in `/usr/local/lib/` on OSX/Linux or in `C:\Program Files\nodejs` on Windows. To uninstall the packages, you can always delete the directory `node_modules` there.

**OSX/Linux:**

	sudo npm install -g grunt-cli gulp bower webpack webpack-dev-server jspm 
	sudo npm install -g yo http-server live-server karma-cli 



**Windows:** run the following commands in a Administrator command prompt.  

	npm install -g grunt-cli gulp bower webpack webpack-dev-server jspm 
	npm install -g yo http-server live-server karma-cli 

	
Ignore some scary warnings about missing Python, these are only for optional features ...


### Browser
A recent Chrome and/or Firefox Browser must be available.  
If you are using Chrome, the following extensions make your life easier:

- [Postman Extension](https://chrome.google.com/webstore/detail/postman-rest-client-packa/fhbjgbiflinjbdggehcddcbncdddomop?hl=en)
- [JetBrains IDE Support](https://chrome.google.com/webstore/detail/jetbrains-ide-support/hmhgeddbohgjknpmjagkdomcpobmllji)
- [AngularJS Batarang](https://chrome.google.com/webstore/detail/angularjs-batarang/ighdmehidhipcmcojjgiloacoafjmpfk?hl=en)
- [ng-inspector for AngularJS](https://chrome.google.com/webstore/detail/ng-inspector-for-angularj/aadgmnobpdmgmigaicncghmmoeflnamj?hl=en)


### WebStorm
The WebStorm IDE from JetBrains should be installed. 
There is a free 30 day trial version of WebStorm available here: <http://www.jetbrains.com/webstorm/>.

WebStorm is not a requirement for the workshop. However the examples and demos will be shown with WebStorm. It is up to the attendees to use any other editor of their preference.  
IntelliJ IDEA supports the same features as WebStorm.


## Preparations

To reduce the network traffic during the course the following steps should be executed before the workshop:

Check out the following git repository:  
`git clone https://bitbucket.org/jonasbandi/ng-digicomp.git`

From within the cloned repository execute the following commands:

	cd 01-Intro/02-SimplisticToDo-Gulp
	npm install
	...
	bower install
	...

The above step did download all the dependencies to build and run the demo application.  
The build should now work by running the following command:

	gulp

After a successful build you can run the project with:

	gulp serve

A browser should open with a simple ToDo-Application.