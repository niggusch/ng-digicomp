To start the exmple:

    npm install
    npm start

The TypeScript transpilation occurs in the browser, so the TypeScript compiler is not needed.
If you use WebStorm and it prompts to compile the files for you, just say "no".