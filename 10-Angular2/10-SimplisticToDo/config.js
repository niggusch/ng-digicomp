System.config({
    baseURL: "/",
    defaultJSExtensions: true,
    transpiler: "typescript",
    typescriptOptions: {
        "emitDecoratorMetadata": true
    },
    packages: {
        "src": {
            defaultExtension: 'ts',
            format: 'es6'
        }
    }
});
