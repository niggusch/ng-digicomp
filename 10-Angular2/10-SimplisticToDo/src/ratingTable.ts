import {Component, NgFor} from "angular2/angular2";
import {RatingService} from "./ratingService";

@Component({
    selector: 'rating-table',
    directives: [NgFor],
    templateUrl: 'src/ratingTable.html'
})
export class RatingTable{
    constructor(public ratingService:RatingService){
    }

    onRemove(rating){
        this.ratingService.ratings.splice(this.ratingService.ratings.indexOf(rating), 1);
    }
}
