export class RatingService{
    ratings:any[] = [
        {name: 'Jonas', grade:'4', courseDate: new Date()},
        {name: 'Erik', grade:'2', courseDate: new Date()}
    ];

    addRating(value:any):void {
        this.ratings.push(value);
    }
}