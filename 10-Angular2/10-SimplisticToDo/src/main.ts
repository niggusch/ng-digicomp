import {bootstrap, Component, View, FORM_DIRECTIVES, NgFor} from "angular2/angular2";
import {RatingTable} from "./ratingTable";
import {RatingService} from "./ratingService";

@Component({
    selector: 'app',
    directives: [FORM_DIRECTIVES, NgFor, RatingTable],
    templateUrl: 'src/main.html'
})
class App{

    name;
    grade;
    ratings = [];

    constructor(public ratingService:RatingService){
    }

    onAdd(rating){
        console.log('Adding ...', rating);
        rating.courseDate = new Date();
        //this.ratings.push(rating);
        this.ratingService.addRating(rating);
        this.name = '';
        this.grade = '';
    }

    onRemove(rating){
        this.ratings.splice(this.ratings.indexOf(rating), 1);
    }
}

bootstrap(App, [RatingService]);
