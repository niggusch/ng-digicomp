(function() {
    'use strict';
    $('#addBtn').on('click', addText);
    $('#do').on('click', '.remove', removeItem);
    updateTotal();

    function addText() {
        var input = $('#input');
        $('#do').append(
            '<li class="clearfix">' +
                input.val() +
                '<span class="pull-right">' +
                '<button class="btn btn-xs btn-danger remove glyphicon glyphicon-trash"></button>' +
                '</span>' +
            '</li>'
        );

        input.val('');
        updateTotal();
    }

    function removeItem() {
        $(this).parents('li').remove();
        updateTotal();
    }

    function updateTotal() {
        $('#total').text($('#do').find('li').length);
    }
})();
