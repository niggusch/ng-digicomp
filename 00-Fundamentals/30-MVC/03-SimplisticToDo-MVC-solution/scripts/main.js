(function() {
    'use strict';

    var model = {
        todos: [],
        newTodo: {text: ''}
    };

    MicroEvent.mixin(TodoView);
    function TodoView() {

        var self = this;

        self.todoInputElem =  document.getElementById('input');
        self.addBtn = document.getElementById('addBtn');

        self.todoInputElem.addEventListener('blur', function(){
            self.trigger('itemChanged', self.todoInputElem.value);
        });

        self.addBtn.addEventListener('click', function(){
            self.trigger('itemAdded');
        });

        self.render = function(todo){
            self.todoInputElem.value = todo.text;
        }
    }

    MicroEvent.mixin(TodoListView);
    function TodoListView() {

        var self = this;

        self.todoCountElem = document.getElementById('total');
        self.todoListElem = document.getElementById('do')

        self.render = function(todos){
            var todo;
            self.todoCountElem.text = todos.length;

            self.todoListElem.innerHTML = '';
            for(var i = 0, len = todos.length; i < len  ; i++ ){
                todo = todos[i];

                var li = document.createElement('li');
                li.setAttribute('data-index', i);
                li.classList.add('clearfix');
                li.textContent = todo.text;

                var span = document.createElement('span');
                span.classList.add('pull-right');

                var button = document.createElement('button');
                button.setAttribute('class', "btn btn-xs btn-danger remove glyphicon glyphicon-trash");
                button.addEventListener('click', (function(index){
                   return function(){self.trigger('itemRemoved', index)};
                })(i));

                span.appendChild(button);
                li.appendChild(span);
                self.todoListElem.appendChild(li);
            }
        }
    }

    var controller = {

        init : function() {

            var todoView = new TodoView();
            var todoListView = new TodoListView();

            todoView.bind('itemChanged', updateTodo);
            todoView.bind('itemAdded', addItem);
            todoListView.bind('itemRemoved', removeItemAtIndex);

            todoView.render(model.newTodo);
            todoListView.render(model.todos);


            function updateTodo(todoText) {
                model.newTodo.text = todoText;
                model.newTodo.created = new Date();
            }
            function addItem(){
                model.todos.push(model.newTodo);
                model.newTodo = {text: ''};
                todoView.render(model.newTodo);
                todoListView.render(model.todos);
            }
            function removeItemAtIndex(index){
                model.todos.splice(index, 1);
                todoListView.render(model.todos);
            }

        }
    };
    controller.init();
})();
