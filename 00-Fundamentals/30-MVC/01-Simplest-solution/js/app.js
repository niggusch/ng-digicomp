/* ======= Model ======= */

var model = {
    currentFramework: null,
    frameworks: [
        {
            likeCount: 0,
            name: 'Backbone',
            imgSrc: 'img/backbone.jpg',
            url: 'http://backbonejs.org/'
        },
        {
            likeCount: 0,
            name: 'Marionette',
            imgSrc: 'img/marionette.png',
            url: 'http://marionette.com/'
        },
        {
            likeCount: 0,
            name: 'EmberJS',
            imgSrc: 'img/ember.jpg',
            url: 'http://emberjs.com/'
        },
        {
            likeCount: 0,
            name: 'Knockout',
            imgSrc: 'img/knockout.jpg',
            url: 'http://knockoutjs.com/'
        },
        {
            likeCount: 0,
            name: 'AngularJS',
            imgSrc: 'img/angular.jpg',
            url: 'https://angularjs.org/'
        },
        {
            likeCount: 0,
            name: 'React',
            imgSrc: 'img/react.png',
            url: 'http://facebook.github.io/react/'
        }

    ]
};


/* ======= View ======= */

MicroEvent.mixin(FrameworkView);
function FrameworkView() {

    var self = this;

    self.frameworkNameElem = document.getElementById('framework-name');
    self.logoImageElem = document.getElementById('logo-img');
    self.likeCountElem = document.getElementById('like-count');
    self.likeButtonElem = document.getElementById('like-button');
    self.frameworkUrlElem = document.getElementById('framework-url');

    self.likeButtonElem.addEventListener('click', function () {
        self.trigger('like');
    });

    self.render = function (currentFramework) {
        self.likeCountElem.textContent = currentFramework.likeCount;
        self.frameworkNameElem.textContent = currentFramework.name;
        self.logoImageElem.src = currentFramework.imgSrc;
        self.frameworkUrlElem.href = currentFramework.url;
        self.frameworkUrlElem.text = currentFramework.url;
    }
}

MicroEvent.mixin(FrameworkListView);
function FrameworkListView() {

    var self = this;

    self.frameworkListElem = document.getElementById('framework-list');

    self.render = function (frameworks) {
        var framework, elem, i;

        // Clear the list
        self.frameworkListElem.innerHTML = '';

        for (i = 0; i < frameworks.length; i++) {
            framework = frameworks[i];

            elem = document.createElement('li');
            elem.textContent = framework.name;

            elem.addEventListener('click', (function (framework) {
                // Capture context in a closure
                return function () {
                    self.trigger('item-selected', framework);
                };
            })(framework));

            self.frameworkListElem.appendChild(elem);
        }
    }
}


/* ======= Controller ======= */

var controller = {

    init: function () {
        // set initial framework
        model.currentFramework = model.frameworks[0];

        var frameworkListView = new FrameworkListView();
        var frameworkView = new FrameworkView();

        frameworkListView.bind('item-selected', setCurrentFramework);
        frameworkView.bind('like', incrementLikes);

        frameworkListView.render(model.frameworks);
        frameworkView.render(model.currentFramework);

        function setCurrentFramework(framework) {
            // update state in the model
            model.currentFramework = framework;
            frameworkView.render(model.currentFramework);
        }

        function incrementLikes() {
            // increments the likes for the currently-selected framework
            model.currentFramework.likeCount++;
            frameworkView.render(model.currentFramework);
        }
    }
};

// make it go!
controller.init();
