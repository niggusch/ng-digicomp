/*global $, describe, beforeEach, afterEach, it, expect, addText, registerButtonHandler */
describe('ToDo List', function(){
    'use strict';

    beforeEach(function() {

        // Programmatically set up dom for test
        var app = $('<div id=app/>');
        var input = $('<input type="text" id="input"/>');
        var addBtn = $('<a id="addBtn" class="btn btn-primary btn-large" >Add &raquo;</a>');
        var todoList = $('<p id="do"> </p>');
        app.append(input);
        app.append(addBtn);
        app.append(todoList);
        $(document.body).html(app);

        // Alternative: Set up dom with html template
        //document.body.innerHTML = window.__html__['src/test/form.html'];

        registerButtonHandler();
    });

    afterEach(function() {
        //console.log($(document.body).html());
    });

    it('should extend list when adding item ', function() {
        //console.log('First test ...');
        var input = document.getElementById('input');
        input.value = 'First ToDo';

        addText();

        var todoListItems = document.getElementById('do').children;
        expect(todoListItems.length).toBe(1);
        expect($(todoListItems[0]).text()).toBe('First ToDo'); // Using jQuery since IE8 only supports 'innerText' and FF only supports 'textContent'
    });

//    it('should add item when clicking button', function() {
//        var input = $('input');
//        input.val('First ToDo');
//
//        var addBtn = $('#addBtn');
//        addBtn.trigger('click'); // in order for this to work the click handler has to be registered through jQuery
//
//        var itemCount = $('#do li').length;
//        expect(itemCount).toBe(1);
//    });
});


