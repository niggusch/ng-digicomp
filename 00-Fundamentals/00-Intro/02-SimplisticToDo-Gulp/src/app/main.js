window.onload = function(){
    'use strict';
    registerButtonHandler();
};

function registerButtonHandler(){
    'use strict';
    var addBtn = document.getElementById('addBtn');
    addBtn.addEventListener('click', addText);
}

function addText(){
    'use strict';
    var input = document.getElementById('input');
    var node=document.createElement('li');
    var textnode=document.createTextNode(input.value);
    node.appendChild(textnode);
    document.getElementById('do').appendChild(node);
    input.value = '';
}
