# Dependency Management

Install all build dependencies and front-end assets of the project:

	npm install
	bower install
	
# Run the Build (linting, asset processing)

	gulp
	
Inspect the result in `dist`


# Run the Unit Tests

	gulp test
	
Or run Karma in continuous mode:

	npm test
	
Or run Karma from WebStrom.


# Run the End-to-End Tests

Install selenium locally in your project:

	npm run selenium:install
	
This runs the command `webdriver-manager update` from within your `node_modules/.bin` which downloads the Selenium server and Chrome driver into `node_modules/protractor/selenium`.


Now open two terminals:


### 1. Run the selenium server

	npm selenium:run 
	
This calls the command `webdriver-manager start` from within your `node_modules/.bin`, which starts the Selenium server.


### 2. Serve the app and run the protractor test

	gulp e2e
	
This runs `gulp serve` to start the application at `http://localhost:8000` and then runs the command `protractor src/e2e/protractor.conf.js` from within your `node_modules/.bin`.
	

## Option: Running IE11

Edit `package.json`, so that IEDriver (32bit) is installed with selenium:

	webdriver-manager update --ie32
	

Edit src/e2e/protractor.conf to run IE:

	'browserName': 'internet explorer'

Note there are problems with the 64bit IEDriver (very slow response): https://code.google.com/p/selenium/issues/detail?id=5116


## Option: Let protractor start/stop the selenium server
If you want to omit step 2, protractor can be configured to automatically start/stop the selenium server.

For this install protractor locally in your project (in this example already done via package.json).

Then also install selenium locally by running the local webdriver-manager:

	./node_modules/protractor/bin/webdriver-manager update 
	
Now you should have the selenium server jar locally in your project:

	./node_modules/protractor/selenium/selenium-server-standalone-2.45.0.jar
	
(version number may be different).

Adjust the file `e2e/protractor.conf.js`:
- Comment out `seleniumAddress`
- Comment in `seleniumServerJar` and set the correct path from above.
