
// Set up DOM interaction with module 1
(function() {
    var m1 = module1();
    $('#LoadButton').click(function () {
        $('#txtValue1').val('');
        $('#txtValue2').val('');
        var settings = m1.loadSettings();
        $('#txtValue1').val(settings[0]);
        $('#txtValue2').val(settings[1]);
    });
    $('#SubmitButton').click(function () {
        m1.storeSettings();
    });
    $('#ClearButton').click(function () {
        localStorage.clear();
        m1.loadSettings();
    });
})();

// Set up DOM interaction with module 2
(function() {
    var m2 = module2();
    $('#LoadButton2').click(function () {
        $('#txtValue3').val('');
        $('#txtValue4').val('');
        var settings = m2.loadSettings();
        $('#txtValue3').val(settings[0]);
        $('#txtValue4').val(settings[1]);
    });
})();