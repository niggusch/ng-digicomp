(function () {

    var myapp = angular.module('myapp', ["ui.router"]);
    myapp.config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/first");

        $stateProvider
            .state('first', {
                url: "/first",
                templateUrl: "templates/first.html"
            })
            .state('second', {
                url: "/second",
                templateUrl: "templates/second.html",
                controller: function () {
                    var vm = this;

                    vm.message = "Hello Universe!";
                },
                controllerAs: 'vm'
            })
            .state('third', {
                url: "/third",
                templateUrl: "templates/second.html",
                controller: "ThirdController as vm"
            })
            .state('fourth', {
                url: "/fourth/:id/:other",
                templateUrl: "templates/second.html",
                controller: "FourthController as vm"
            })
            .state('fifth', {
                url: "/fifth",
                templateUrl: "templates/second.html",
                controller: "FifthController as vm",
                resolve: {
                    introMsg: function () {
                        return "Fifth message: ";
                    },
                    data: ['$http', function ($http) {
                        return $http.get('https://api.github.com/repos/' +
                        'angular/angular.js/commits');
                    }]
                }
            });
    });

})();