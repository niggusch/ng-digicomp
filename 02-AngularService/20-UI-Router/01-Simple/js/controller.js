(function() {

    var myapp = angular.module('myapp');

    myapp.controller('Controller', function(){
        var vm = this;

        vm.message = "Hello World!";
    });

    myapp.controller('ThirdController', function(){
        var vm = this;

        vm.message = "Hello Everything!";
    });

    myapp.controller('FourthController', ['$stateParams', function($stateParams){
        var vm = this;

        vm.message = 'The Id is ' + $stateParams.id;
        vm.other = 'The other is ' + $stateParams.other;
    }]);

    myapp.controller('FifthController', ['introMsg', 'data', function(introMsg, data){
        var vm = this;

        vm.message = introMsg + angular.toJson(data);
    }]);

})();
