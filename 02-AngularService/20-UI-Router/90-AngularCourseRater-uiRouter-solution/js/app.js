var ratingApp = angular.module('RatingApp', ['ui.router']);

ratingApp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/ratings");

    $stateProvider
        .state('ratings', {
            url: "/ratings",
            templateUrl: "partials/ratings.html",
            controller: "RatingCtrl as vm"
        })
        .state('users', {
            url: "/users",
            templateUrl: "partials/users.html",
            controller: "UserCtrl as vm",
            //resolve: {
            //     test: function(){  return new Promise(function(resolve, reject){
            //        setTimeout(function(){resolve();}, 2000)
            //     });}
            //}
        });
});

ratingApp.controller("RatingCtrl", ['RatingService','UserService', function (ratingService, userService) {
    var vm = this;

    vm.ratings = ratingService.getRatings();

    vm.usernames = _.map(userService.getUsers(), 'name');

    vm.pluralizer = {
        0: "No rating!",
        1: "Only one rating!",
        other: "{} ratings."
    };

    vm.addRating = function () {
        ratingService.addRating({name: vm.name, rating: vm.rating, entered: new Date()});
    };

    vm.removeRating = function (rating) {
        if (confirm("Remove this rating. Sure?")) {
            ratingService.removeRating(rating);
            vm.ratings = ratingService.getRatings();
        }
    }
}]);

ratingApp.controller("UserCtrl", ['UserService', function (userService) {
    var vm = this;

    vm.newUser = {name: '', email: ''};
    vm.users = userService.getUsers();

    vm.addUser = function () {
        userService.addUser(vm.newUser);
        vm.users = userService.getUsers();
        vm.newUser = {name: '', email: ''};
    };

    vm.updateUser = function (user) {
        console.log('Updating user!');
    };

    vm.removeUser = function (user) {
        userService.removeUser(user);
        vm.users = userService.getUsers();
    };
}]);

ratingApp.factory("UserService", function () {

    var users = [
        {name: 'Will Hunting', email: "test@test.com"}
    ];

    return {
        getUsers: function () {
            return users
        },
        addUser: function (user) {
            users.push(user)
        },
        removeUser: function (user) {
            users = _.without(users, user);
        }
    }
});

ratingApp.factory("RatingService", function () {

    var ratings = [
        {name: 'Will Hunting', rating: 3.50, entered: "2013-08-05"}
    ];

    return {
        getRatings: function () {
            return ratings
        },
        addRating: function (rating) {
            ratings.push(rating)
        },
        removeRating: function (rating) {
            ratings = _.without(ratings, rating);
        }
    }
});

ratingApp.controller("AppCtrl", ['$state', '$rootScope', function ($state, $rootScope) {
    var vm = this;


    vm.goToRatings = function () {
        $state.go('ratings')
    };

    vm.goToUsers = function () {
        $state.go('users')
    };

    $rootScope.$on('$stateChangeStart', function(){
        console.log('$stateChangeStart');
        vm.showprogress = true;
    });

    $rootScope.$on('$stateChangeSuccess', function(){
        console.log('$stateChangeSuccess');
        vm.showprogress = false;
    });

}]);
