var myApp = angular.module('myApp',[]);

myApp.controller('FirstCtrl', ['FirstService', '$scope', function(MyService, $scope) {
    $scope.property = MyService.theNumber;
    $scope.property2 = MyService.getValue();
}]);

myApp.controller('SecondCtrl', ['FirstService', '$scope', function(MyService, $scope) {
    $scope.property = MyService.theNumber;
    $scope.property2 = MyService.getValue();
}]);

myApp.factory('FirstService', function() {

    function getOtherValue(){
        return 43;
    }

    return {
        theNumber: 42,
        toUpercase: function(input){ return input.toUpercase() },
        getValue: getOtherValue
    }
});
