var myApp = angular.module('myApp',[]);

myApp.controller('firstCtrl', ['myService', function(myService) {

    var vm = this;

    var v1 = 41;
    var v2 = 2;

    // First async API call
    var promise1 = myService.addAsync(v1, v2);
    promise1.then(function(result){
        vm.result = result;
    });

    // Second async API call
    var promise2 = myService.getDataAsync();
    promise2.then(function(data){
       vm.data = data;
    });
    promise2.catch(function(error){
       vm.data = "Sorry! We are unable to fulfil your request." + error;
    });

    // Third async API call
    var promise3 = myService.getMoreDataAsync();
    promise3.then(function(data){
        vm.moreData = data;
    });
    promise3.catch(function(error){
        vm.moreData = "Sorry! We are unable to fulfil your request." + error;
    });
}]);


myApp.factory('myService', ["$q", "$http" , function($q, $http) {

    var BACKEND_URL = 'https://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors';
    //var BACKEND_URL = 'http://localhost:3456/courserater/rest/ratings';

    return {
        addAsync : addAsync,
        getDataAsync : getDataAsync,
        getMoreDataAsync : getMoreDataAsync
    };

    function addAsync(val1, val2) {

        var promise = $q(function(resolve, reject) {
            setTimeout(function () {
                var result = val1 + val2;
                resolve(result);
            })
        });
        return promise;
    }

    // You can chain transformations on the original promise like this:
    function getDataAsync(){
        return $http.get(BACKEND_URL)
            .then(returnDataFromResponse) // Probably it is not sensible to return this promise to the controller
            .catch(logError); // Catching the error here will not let you catch it in the controller
            //.catch(throwError); // Throwing will reject the promise
    }

    // Another technique is to explicitly implement a promise to return to the controller:
    function getMoreDataAsync(){
        return $q(function(resolve,reject) {
            $http.get(BACKEND_URL)
                .then(function(response){resolve(response.data)})
                .catch(function(error){reject("Error details: " + error)});
        });
    }


    function returnDataFromResponse(response){
        return response.data; // Here we could also create a custom object an fill its properties
    }

    function logError() {
        console.log("Something went wrong!");
    }

    function throwError() {
        throw "You will not understand the details anyway!"
    }
}]);

