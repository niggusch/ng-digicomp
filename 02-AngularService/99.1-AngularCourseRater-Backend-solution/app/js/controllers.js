(function(){

    var ratingApp = angular.module('RatingApp');

    ratingApp.controller('RatingCtrl',
        ['$window', 'RatingBackend',
            function ($window, ratingBackend) {

        var vm = this;

        var loadRatings = function () {

            var promise = ratingBackend.getAllRatings();
            promise.then(function (data) {
                vm.ratings = data;
            })
                .catch(function(){handleError()});
        };

        vm.addRating = function () {
            var rating = {participantName: vm.participantName, grade: vm.grade, courseDate: new Date()};
            vm.ratings.push(rating);
            vm.participantName = '';
            vm.grade = '';

            ratingBackend.addRating(rating)
                .catch(function () {
                    handleError();
                });
        };

        vm.removeRating = function (rating) {
            if (confirm("Remove this rating. Sure?")) {
                var index = vm.ratings.indexOf(rating);
                vm.ratings.splice(index, 1);

                ratingBackend.deleteRating(rating)
                    .catch(function () {
                        handleError();
                    });
            }
        };

        var handleError = function () {
            alert("Something went wrong!");
            $window.location.reload();
        };

        vm.pluralizer = {
            0: "No rating!",
            1: "Only one rating!",
            other: "{} ratings."
        };

        // initial load
        loadRatings();
    }])
})();
