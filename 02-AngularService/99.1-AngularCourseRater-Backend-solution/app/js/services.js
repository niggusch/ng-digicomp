(function() {
    var ratingApp = angular.module('RatingApp');

    ratingApp.factory('RatingBackend', ['BACKEND_URL', '$http', function (BACKEND_URL, $http) {

        return {
            getAllRatings: getAllRatings,
            addRating: addRating,
            deleteRating: deleteRating
        };

        function getAllRatings() {
            return $http.get(BACKEND_URL)
                .then(function (response) {
                    return response.data;
                })
                .catch(function (response) {
                    return response.status;
                });
        }

        function addRating(rating) {
            return $http.post(BACKEND_URL, rating)
                .then(function (response) {
                    rating.ratingId = response.data.ratingId; // update rating with sever-generated id
                    return;
                })
                .catch(function (response) {
                    return response.status;
                }
            );
        }

        function deleteRating(rating) {
            return $http.delete(BACKEND_URL + '/' + rating.ratingId)
                .then(function (response) {
                    return;
                })
                .catch(function (response) {
                    return response.status;
                });
        }

    }])
})();


//(function () {
//    var ratingApp = angular.module('RatingApp');
//
//    ratingApp.factory('RatingBackend', ['BACKEND_URL', '$http', '$q', function (BACKEND_URL, $http, $q) {
//
//        return {
//            getAllRatings: getAllRatings,
//            addRating: addRating,
//            deleteRating: deleteRating
//        };
//
//        function getAllRatings() {
//            return $q(function (resolve, reject) {
//
//                $http.get(BACKEND_URL)
//                    .then(function (response) {
//                        resolve(response.data);
//                    })
//                    .catch(function (response) {
//                        reject(response.status);
//                    });
//            });
//        }
//
//        function addRating(rating) {
//
//            return $q(function (resolve, reject) {
//
//                $http.post(BACKEND_URL, rating)
//                    .then(function (response) {
//                        rating.id = response.data.ratingId; // update rating with sever-generated id
//                        resolve();
//                    })
//                    .catch(function (response) {
//                        reject(response.status);
//                    }
//                );
//            });
//        }
//
//        function deleteRating(rating) {
//
//            return $q(function (resolve, reject) {
//
//                $http.delete(BACKEND_URL + '/' + rating.id)
//                    .then(function (response) {
//                        resolve();
//                    })
//                    .catch(function (response) {
//                        reject(response.status);
//                    });
//
//            });
//        }
//
//    }])
//})();



