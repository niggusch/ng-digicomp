(function() {

    var ratingApp = angular.module('RatingApp', []);

    //ratingApp.constant('BACKEND_URL', 'http://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors');
    ratingApp.constant('BACKEND_URL', 'http://localhost:3456/courserater/rest/ratings');

})();
