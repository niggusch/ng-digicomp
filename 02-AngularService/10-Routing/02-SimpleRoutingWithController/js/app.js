var myApp = angular.module('myApp', [
    'ngRoute'
]);

myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/1', {
                templateUrl: 'partials/page1.html',
                controller: 'Page1Controller as vm'
            }).
            when('/2', {
                templateUrl: 'partials/page2.html',
                controller: 'Page2Controller'
            }).
            otherwise({
                redirectTo: '/1'
            });
    }]);

myApp.controller('Page1Controller', function(){
        var vm = this;
        console.log("Page1Controller");
        vm.property = "41";
    }
)

myApp.controller('Page2Controller', function($scope){
        console.log("Page2Controller");
        $scope.property = "42";
    }
)