
var myApp = angular.module('myApp',[]);

myApp.controller('MyCtrl', ['$http', function($http) {

    var vm = this;

    //var backendUrl = 'http://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors';
    var backendUrl = 'http://localhost:3456/courserater/rest/ratings';

    var getRatings = function() {
        $http.get(backendUrl)
            .then(function (response) {
                vm.status = response.status;
                vm.ratings = response.data;
            })
            .catch(function (error) {
                // Note that status is only set if the server responded with a status.
                // In the case of CORS problems, the browser is raising the error and no status set here!
                alert("Could not get data from server! Status: " + error.status)
            });
    };

    var postRating = function() {
        $http.post(backendUrl, {"participantName": vm.participantName, "score": vm.score, "courseDate": new Date()});
    };

    var putRating = function() {
        $http.put(backendUrl + '/' + vm.ratingId, {"id":vm.ratingId , "participantName": vm.participantName, "score": vm.score, "courseDate": new Date()})
    };

    var deleteRating = function() {
        $http.delete(backendUrl + '/' + vm.ratingId);
    };


    vm.getRatings = getRatings;
    vm.postRating = postRating;
    vm.putRating = putRating;
    vm.deleteRating = deleteRating;
}]);


