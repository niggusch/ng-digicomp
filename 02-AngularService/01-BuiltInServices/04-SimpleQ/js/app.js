"use strict";
(function() {

    var myApp = angular.module('myApp', []);

    myApp.controller('MyCtrl', ['$q', function ($q) {

        var vm = this;

        var getValueAsync = function () {
            var promise = $q(function (resolve, reject) {
                setTimeout(function () {
                    resolve("This is the promised value!");
                }, 3000);
            });

            return promise;
        };


        var myPromise = getValueAsync();
        myPromise.then(function (value) {
            vm.message = value;
        });
        myPromise.catch(function (error) {
            console.log("An error occured: " + error);
        });

    }]);

})();

