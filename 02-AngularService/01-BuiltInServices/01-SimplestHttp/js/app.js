
var myApp = angular.module('myApp',[]);

myApp.controller('MyCtrl', ['$http', function($http) {

    var vm = this;

    //var BACKEND_URL = 'https://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors';
    var BACKEND_URL = 'http://localhost:3456/courserater/rest/ratings';

    $http.get(BACKEND_URL)
        .then(function(response){
            vm.status = response.status;
            vm.data = response.data;
        })
        .catch(function(error){
            // Note that status is only set if the server responded with a status.
            // In the case of CORS problems, the browser is raising the error and no status set here!
            alert("Could not get data from server! Status: " + error.status)
        });

    console.log("Getting data ...");
}]);


