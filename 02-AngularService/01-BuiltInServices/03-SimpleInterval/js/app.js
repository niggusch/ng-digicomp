
var myApp = angular.module('myApp',[]);

myApp.controller('MyCtrl', ['$http', '$interval', function($http, $interval) {

    var vm = this;

    //var BACKEND_URL = 'https://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors';
    var BACKEND_URL = 'http://localhost:3456/courserater/rest/ratings';

    var fetchData = function () {
        $http.get(BACKEND_URL)
            .success(function (data, status, headers, config) {
                vm.status = status;
                vm.data = data;
            });

        console.log("Getting data ...");
    };

    var decrementCountdown = function(){
        vm.countdown -= 1;
        console.log("Counter is " + vm.countdown);

        if (vm.countdown < 1) {
            fetchData();
        }
    };

    var startCountdown = function(){
        $interval(decrementCountdown, 1000, vm.countdown);
    };

    vm.countdown = 5;
    vm.getData = fetchData;
    startCountdown();
}]);


