(function () {

    var crApp = angular.module("crApp", []);

    crApp.controller("RatingsCtrl", function () {

        var vm = this;

        vm.rating = {};
        vm.ratings = [
            {participantName: 'Will Hunting', grade: 3.50, courseDate: "2013-08-05"}
        ];

        vm.addRating = function () {
            vm.ratings.push({participantName: vm.rating.participantName, grade: vm.rating.grade, courseDate: new Date()});
            //vm.ratings.push(vm.rating);
            vm.rating = {};
        };

        vm.removeRating = function (item) {
            if (confirm("Remove this rating. Sure?")) {
                vm.ratings.splice(vm.ratings.indexOf(item), 1);
            }
        }

        vm.pluralizer = {
            0: "No rating!",
            1: "Only one rating!",
            other: "{} ratings."
        };
    });

})();
