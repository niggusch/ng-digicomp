var express = require('express');
var morgan = require('morgan');
var _ = require('lodash');


var app = express();
var util = require('util');

var ratings = [{ratingId:0, "participantName": "John","score": "4", "courseDate":"2014-04-15"}];
var nextId = 1;

//app.use(express.static(__dirname));
app.listen(3456);
app.use(express.json());
app.use(express.urlencoded());
app.use(morgan('combined')); // configure default log output
util.puts('Server running at http://localhost:3456');

app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
//    res.header("Access-Control-Allow-Headers, Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});

app.get('/test', function (req, res) {
    res.status(200).send("Hello World!");
});

app.get('/courserater/rest/ratings', function (req, res) {
    res.status(200).json(ratings);
});

app.post('/courserater/rest/ratings', function (req, res) {
    if (!req.is('json')) {
        res.status(415).send('Payload must be JSON');
    }
    util.puts(util.inspect(req.body));

    var newRating = req.body;
    newRating.ratingId = nextId;
    ratings.push(newRating);

    nextId++;

    util.puts(util.inspect(ratings));

    res.status(201).json(newRating);
//    res.status(500).json(newRating); // return an error to see how the client behaves...
});

app.delete('/courserater/rest/ratings/:id', function(req, res){

    var id = req.param("id");
    util.puts(id);

    var index =_.findIndex(ratings, function(e) {return e.ratingId === id});

    ratings.splice(index,1);
    res.status(204).send();
})