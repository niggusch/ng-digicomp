(function () {

    var ratingApp = angular.module('RatingApp', []);

    ratingApp.controller("RatingCtrl", function () {
        var vm = this;
        vm.ratings = [
            {name: 'Will Hunting', rating: 3.50, entered: "2013-08-05"}
        ];

        vm.addRating = function () {
            vm.ratings.push({name: vm.name, rating: vm.rating, entered: new Date()});
        };

        vm.removeRating = function (item) {
            if (confirm("Remove this rating. Really Sure? " + item.rating)) {
                vm.ratings.splice(vm.ratings.indexOf(item), 1);
            }
        }
    });


    ratingApp.directive('tableCounter', function () {
        return {
            restrict: 'E',
            template: '<h3>Count: {{collection.length}}</h3>',
            scope: {collection: '='}
        }
    });

    ratingApp.directive('ratingList', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/rating-list.html',
            scope: {},
            controller: function () {
                var vm = this;

                vm.removeRating = function (item) {

                    // First solution: directive manages removal
                    if (confirm('Remove this rating. Sure?')) {
                        vm.ratings.splice(vm.ratings.indexOf(item), 1);
                    }

                    // Second solution: parent manages removal, call/parameter is part of the expression
                    //vm.remove({item: item});

                    // Third solution: parent manages removal, no function call in the expression
                    // -> 'remove' is a function that returns the actual parent function
                    //var fn = vm.remove();
                    //fn(item);
                }
            },
            controllerAs: 'vm',
            bindToController: {
                ratings: '=',
                remove: '&'
            }
        }
    });

})();
