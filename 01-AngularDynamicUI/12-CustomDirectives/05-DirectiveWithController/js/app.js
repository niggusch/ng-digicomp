(function () {

    var app = angular.module('myApp', []);

    app.controller('Controller', function () {
        var vm = this;
        vm.firstRating = {name:'Jonas', grade:4};
        vm.secondRating = {name:'Bandi', grade:3};
        vm.testValue = 'Test';
    });

    app.directive('myRating', function(){
        return {
            restrict: 'E',
            templateUrl: 'templates/my-rating.html',
            scope: {}, // isolated scope
            controller: function(){
                var vm = this;

                vm.summary = vm.rating.name + " rated " + vm.rating.grade;

                vm.click = function(){ alert ('Hello ' + vm.rating.name); };
            },
            controllerAs: 'vm',
            bindToController: {rating: '='} // properties bound to the controller
        }
    });

})();


