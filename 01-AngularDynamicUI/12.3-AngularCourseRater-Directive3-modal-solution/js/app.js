'use strict';
(function () {

    var ratingApp = angular.module('RatingApp', ['ui.bootstrap']);

    ratingApp.controller("RatingCtrl", function ($modal, $log) {
        var vm = this;
        vm.ratings = [
            {name: 'Will Hunting', rating: 3.50, entered: "2013-08-05"}
        ];

        vm.addRating = function () {
            vm.ratings.push({name: vm.name, rating: vm.rating, entered: new Date()});
        };

        vm.removeRating = function (item) {

            var modalInstance = $modal.open({
                templateUrl: 'templates/modal.html',
                controller: ModalCtrl,
                controllerAs: 'vm'
            });

            modalInstance.result.then(function () {
                vm.ratings.splice(vm.ratings.indexOf(item), 1);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };

        function ModalCtrl($modalInstance){
            var vm = this;

            vm.ok = function () {
                $modalInstance.close();
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }

    });

})();
