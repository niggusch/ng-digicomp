'use strict';
(function () {

    var ratingApp = angular.module('RatingApp');

    ratingApp.directive('ratingList', function () {
        return {
            restrict: 'E',
            templateUrl: 'templates/rating-list.html',
            scope: {},
            controller: function(){
                var vm = this;

                vm.removeRating = function (item) {
                    vm.remove()(item);
                }
            },
            controllerAs: 'vm',
            bindToController:  {ratings: '=', remove: '&'}
        }
    });

})();
