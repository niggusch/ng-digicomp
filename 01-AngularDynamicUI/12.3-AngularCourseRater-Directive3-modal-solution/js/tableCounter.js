'use strict';
(function () {

    var ratingApp = angular.module('RatingApp');

    ratingApp.directive('tableCounter', function () {
        return {
            restrict: 'E',
            template: '<h3>Count: {{collection.length}}</h3>',
            scope: {collection: '='}
        }
    });

})();
