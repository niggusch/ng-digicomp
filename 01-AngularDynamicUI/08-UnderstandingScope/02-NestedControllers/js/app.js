(function () {

    var myApp = angular.module('myApp', []);

    myApp.controller("FirstCtrl", function ($scope) {
        $scope.message = "Hello World!";
    });

    myApp.controller("SecondCtrl", function ($scope) {
        $scope.message = "Hello Universe!";
    });

})();





