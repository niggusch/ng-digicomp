(function () {

    var crApp = angular.module("crApp", []);

    crApp.controller("RatingsCtrl", function () {

        var vm = this;

        vm.rating = {};
        vm.ratings = [
            {name: 'Will Hunting', grade: 3.50, courseDate: "2013-08-05"}
        ];

        vm.addRating = function () {
            vm.rating.courseDate = new Date();
            vm.ratings.push(vm.rating);
            vm.rating = {};
        };

        vm.removeRating = function (rating) {
            if(confirm('Sure?')) {
                vm.ratings.remove(rating);
            }        };

        vm.pluralizer = {
            0: "No rating!",
            1: "Only one rating!",
            other: "{} ratings."
        };

    });

})();
