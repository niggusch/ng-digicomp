(function () {

    var ratingApp = angular.module('RatingApp', []);

    ratingApp.controller("RatingCtrl", function () {
        var vm = this;
        vm.ratings = [
            {name: 'Will Hunting', rating: 3.50, entered: "2013-08-05"}
        ];

        vm.addRating = function () {
            vm.ratings.push({name: vm.name, rating: vm.rating, entered: new Date()});
        };

        vm.removeRating = function (item) {
            if (confirm("Remove this rating. Sure?")) {
                vm.ratings.splice(vm.ratings.indexOf(item), 1);
            }
        }
    });


    ratingApp.directive('tableCounter', function () {
        return {
            restrict: 'E',
            template: '<h3>Count: {{collection.length}}</h3>',
            scope: {collection: '='}
        }
    });

})();