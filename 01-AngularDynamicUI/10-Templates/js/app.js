(function () {

    var myApp = angular.module('myApp', []);

    myApp.controller("FirstCtrl", function () {

        var vm = this;

        vm.dynamicUrl = 'templates/hello.html';

        vm.getDynamicUrl = function(){
            if (new Date().getMilliseconds() % 2 === 0){
                return 'templates/hello.html';
            }
            else {
                return 'templates/bye.html';
            }
        }

    });

})();





