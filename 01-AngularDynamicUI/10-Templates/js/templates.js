(function() {

    var myApp = angular.module('myApp');
    myApp.run(['$templateCache', function ($templateCache) {
        $templateCache.put('templates/hello.html', 'Hello World!');
    }]);

})();