
var myApp = angular.module('myApp', []);

myApp.controller('MyCtrl', function () {

    this.setInitialState = setInitialState;

    function setInitialState() {
        this.item = {
            quantity: 1,
            price: 50,
            getAmount: function () {
                return this.quantity * this.price
            }
        };
    }

    // Remove the prepending this -> What happens? -> How to fix?
    this.setInitialState();

});














//Add property to item object:
//quality: Math.ceil(Math.random()*3),
