var myApp = angular.module('myApp',[]);

myApp.controller("MyCtrl", Controller);
//myApp.controller("MyCtrl", ['$scope','$interval', '$location', Controller]);

Controller.$inject = ['$scope','$interval', '$location'];
function Controller($scope, $interval, $location){

    updateMessage();

    $interval(updateMessage, 1000);

    function updateMessage() {

        var message = 'Current Time is: ';
        message += new Date();

        var message2 = 'Your URL is: ';
        message2 += $location.absUrl();

        $scope.message = message;
        $scope.message2 = message2;
    }

}


































//// Manipulate scope programmatically
//angular.element($('#app')).scope().message
//
//angular.element($("#app")).scope().message = 'Gugus!'
//angular.element($("#app")).scope().$apply()
