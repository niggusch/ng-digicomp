'use strict';
(function() {
    var ratingApp = angular.module('RatingApp');

    //ratingApp.constant('BACKEND_URL', 'http://courserater-jbandi.rhcloud.com/courserater/rest/ratings-cors');
    ratingApp.constant('BACKEND_URL', 'http://localhost:3456/courserater/rest/ratings');

    ratingApp.config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/ratings');

        $stateProvider
            .state('ratings', {
                url: '/ratings',
                templateUrl: 'app/ratings/ratings.html',
                controller: 'RatingsCtrl as vm',
                resolve: {
                    ratingService: 'RatingService',
                    ratings: function(ratingService){
                        return ratingService.loadRatings();
                    }
                }
            })
            .state('users', {
                url: '/users',
                templateUrl: 'app/users/users.html',
                controller: 'UsersCtrl as vm'
            });
    });
})();
