'use strict';
(function(){

    var ratingApp = angular.module('RatingApp');

    ratingApp.factory('RatingService', function (BACKEND_URL, $http, $q) {

        var ratings;

        return {
            loadRatings: loadRatings,
            getRatings: getRatings,
            addRating: addRating,
            deleteRating: deleteRating
        };

        function getRatings() {
            return ratings;
        }

        function loadRatings(){

            var deferred = $q.defer();
            $http.get(BACKEND_URL)
                .then(function (response) {
                    ratings = response.data;
                    deferred.resolve(ratings);
                })
                .catch(function (response) {
                    deferred.reject(response.status);
                });

            return deferred.promise;
        }

        function addRating(rating) {
            ratings.push(rating);
            var deferred = $q.defer();
            $http.post(BACKEND_URL, rating)
                .then(function (response) {
                    var newId = response.headers('Location').split('/').pop();
                    rating.id = newId;
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response.status);
                });
            return deferred.promise;
        }

        function deleteRating(rating) {
            var index = ratings.indexOf(rating);
            ratings.splice(index, 1);

            var deferred = $q.defer();
            $http.delete(BACKEND_URL + '/' + rating.id)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response.status);
                });
            return deferred.promise;
        }

    });
})();
