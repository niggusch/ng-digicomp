'use strict';
(function(){
    var ratingApp = angular.module('RatingApp');

    ratingApp.factory('UserService', function() {

        var users = [
            {name: 'Will Hunting', email: 'test@test.com'}
        ];

        return {
            getUsers: function () {
                return users.slice();
            }, // shallow copy
            addUser: function (user) {
                users.push(user);
            },
            removeUser: function (user) {
                users = _.without(users, user);
            }
        };
    });

})();
