/*global confirm:false */
'use strict';
(function() {
    var ratingApp = angular.module('RatingApp');
    ratingApp.directive('ratingList', function (RatingService) {
        return {
            restrict: 'E',
            templateUrl: 'app/ratings/cr-rating-list.html',
            scope: {ratings: '='},
            controller: function () {
                var vm = this;

                vm.removeRating = function(item){
                    if(confirm('Remove this rating. Sure?')){
                        RatingService.deleteRating(item);
                        vm.ratings = RatingService.getRatings();
                    }
                };
            },
            controllerAs: 'vm',
            bindToController: true
        };
    });
})();