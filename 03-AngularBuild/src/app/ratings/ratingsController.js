'use strict';
(function(){
    var ratingApp = angular.module('RatingApp');

    ratingApp.controller('RatingsCtrl', function(ratings, RatingService , UserService) {
        var vm = this;

        vm.usernames = _.map(UserService.getUsers(), 'name');

        vm.ratings = ratings;


        vm.addRating = function(){
            RatingService.addRating({participantName: vm.name, score: vm.rating, courseDate: new Date()});
            vm.ratings = RatingService.getRatings();
        };

    });
})();
