'use strict';
(function () {

    var ratingApp = angular.module('RatingApp');

    ratingApp.directive('crTableCount', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/common/cr-table-count.html',
            scope: {collection: '='}
        };
    });
})();