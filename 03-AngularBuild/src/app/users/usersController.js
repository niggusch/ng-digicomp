'use strict';
(function(){
    var ratingApp = angular.module('RatingApp');

    ratingApp.controller('UsersCtrl', function(UserService) {
        var vm = this;

        vm.newUser = { name: '', email: '' };
        vm.users = UserService.getUsers();

        vm.addUser = function(){
            UserService.addUser(vm.newUser);
            vm.users = UserService.getUsers();
            vm.newUser = { name: '', email: '' };
        };

        vm.updateUser = function(user){
            console.log('Updating user: ' + user.name);
        };

        vm.removeUser = function(user){
            UserService.removeUser(user);
            vm.users = UserService.getUsers();
        };
    });
})();
